#!/bin/bash
rm -rf cache
helm pull hello --untar --destination cache/. --version 0.1.2 --repo "https://cloudecho.github.io/charts/"
rm -rf bundle/hello
helm template hello cache/hello -f cache/hello/values.yaml -f values.yaml --output-dir bundle/.
